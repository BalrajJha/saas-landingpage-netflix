const signButton = document.getElementsByClassName("sign-button");
// console.log(Array.from(signButton));

Array.from(signButton).map((button) => {
    button.addEventListener("click", (e) => {
        // check if user is signed up then make the button point to dashboard
        // else point to homepage
        // if (sessionStorage.hasOwnProperty("login")) {
        //     window.location.href = './dashboard.html';
        // } else {
        //     window.location.href = './signup.html';
        // }

        window.location.href = "./signup.html";
    })

    return -1;
});



function showWhat() {
    let x = document.getElementById("what");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
function showCost() {
    let x = document.getElementById("cost");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
function showWhere() {
    let x = document.getElementById("where");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
function showCancel() {
    let x = document.getElementById("cancel");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
function showWatch() {
    let x = document.getElementById("watch");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
function showKids() {
    let x = document.getElementById("kids");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

function headerDropdown() {
    document.getElementById("header-dropdown").classList.toggle("show");
}
function footerDropdown() {
    document.getElementById("footer-dropdown").classList.toggle("show");
}
