const form = document.getElementById('form');
const firstName = document.getElementById('first_name');
const lastName = document.getElementById('last_name');
const email = document.getElementById('email');
const password = document.getElementById('password');
const passwordConfirm = document.getElementById('password-confirm');
const tos = document.getElementById('tos');
const invalid = document.getElementById('invalid');
const formHeader = document.getElementById('form-header');
const container = document.getElementsByClassName("container")[0];
const dashboardCta = document.getElementById("dashboard-cta");

/* error text messages*/
const invalidFirstName = document.getElementById('invalid-first_name');
const invalidLastName = document.getElementById('invalid-last_name');
const invalidEmail = document.getElementById('invalid-email');
const invalidPassword = document.getElementById('invalid-password');
const invalidPasswordConfirm = document.getElementById('invalid-password-confirm');
const invalidTos = document.getElementById('invalid-tos');

function hasNumbers(name) {
    let flag = false;
    const specialCharacters = ['~', '!', '@', '#', '$', '%', '*', '(', ')', '+', '=', ':'];
    
    name.split("").map((character) => {
        if ((character >= '0' && character <= '9') || specialCharacters.indexOf(character) > -1) {
            flag = true;
        }

        return "";
    });

    return flag;
}

form.addEventListener('submit', (e) => {
    let hasError = false;

    e.preventDefault();

    if (firstName.value.length === 0 || firstName.value.indexOf(" ") > -1 || hasNumbers(firstName.value)) {
        invalidFirstName.innerText = "Please make sure your first name contains only alphabets with no space";
        firstName.style["border-bottom"] = "3px solid #e87c03";
        hasError = true;
    } else {
        firstName.style["border-bottom"] = "none";
        invalidFirstName.innerText = "";
    }

    if (lastName.value.length === 0 || lastName.value.indexOf(" ") > -1 || hasNumbers(lastName.value)) {
        invalidLastName.innerText = "Please make sure your last name contains only alphabets with no space";
        lastName.style["border-bottom"] = "3px solid #e87c03";
        hasError = true;
    }  else {
        lastName.style["border-bottom"] = "none";
        invalidLastName.innerText = "";
    }


    // regex pattern for email 
    // regex taken from http://emailregex.com/

    const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (pattern.test(email.value) === false) {
        invalidEmail.innerText = "Please enter correct email pattern e.g. yourname@mail.com";
        email.style["border-bottom"] = "3px solid #e87c03";
        hasError = true;
    } else {
        email.style["border-bottom"] = "none";
        invalidEmail.innerText = "";
    }

    if (password.value.length < 8) {
        invalidPassword.innerText = "Please make sure your password has atleast 8 characters";
        password.style["border-bottom"] = "3px solid #e87c03";
        hasError = true;
    } else {
        invalidPassword.innerText = "";
        password.style["border-bottom"] = "none";
    }

    if (passwordConfirm.value.length < 8 || passwordConfirm.value !== password.value) {
        invalidPasswordConfirm.innerText = "Please make sure both passwords match and have atleast 8 characters";
        passwordConfirm.style["border-bottom"] = "3px solid #e87c03";
        hasError = true;
    } else {
        passwordConfirm.style["border-bottom"] = "none";
        invalidPasswordConfirm.innerText = "";
    }


    if (tos.checked === false) {
        invalidTos.innerText = "Please accept our terms and conditions";
        hasError = true;
    } else {
        invalidTos.innerText = "";
    }

    if (hasError) {
        // 
    } else {
        const loginDetails = {
            'firstName': firstName.value,
            'lastName': lastName.value,
        };

        sessionStorage.setItem('login', JSON.stringify(loginDetails));
        
        dashboardCta.style.display = "block";
        dashboardCta.style.textAlign = "center";

        form.style.display = "none";
        formHeader.innerText = "🎉Signup Successful🥳";
        formHeader.style["margin-top"] = "2em";
        container.style["min-height"] = "300px";
    }
});

dashboardCta.addEventListener('click', (e) => {
    window.location.href = "./dashboard.html";
});