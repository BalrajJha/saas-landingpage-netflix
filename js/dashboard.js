const welcomeButton = document.getElementById("welcome-button");
let loginDetails;

try {
    loginDetails = JSON.parse(sessionStorage.getItem('login'));
    welcomeButton.textContent = `Welcome, ${loginDetails.firstName} 👋`;
} catch(e) {
    welcomeButton.textContent = "Welcome, User 👋";
    // console.error(e,"NO user eror");
}



// get data for products
fetch('https://fakestoreapi.com/products')
    .then((res)=> {
        if (res.ok) {
            console.log("Success");
            return res.json();
        } else {
            console.log("Not successful");

            window.location.href = "./error.html";
        }
    })
    .then((json)=> {
        let productData = json;
        const productDataContainer = document.getElementById("product-data");
        
        // productData = [];
        if (productData === null || productData.length === 0) {
            //API successful but no products -> Show a no products message

            // remove the loader animation
            productDataContainer.classList.remove("loader");

            const noProductMessage = document.getElementById("no-product-message");
            noProductMessage.textContent = "No products to show 😞";
            
            const homeButton = document.querySelector("main > button");

            homeButton.style.display = "block";

        } else {
            // Products are loaded -> Render the products
            productData.map((product) => {
                // create a div
                const card = document.createElement("div");
                card.classList.add("card");
    
                // create image element
                const img = document.createElement("img");
                img.src = product['image'];
                img.classList.add("card-image");
    
                // create header for category
                const category = document.createElement("h2");
                category.innerText = product["category"];
    
                // create header for title
                const title = document.createElement("h3");
                title.innerText = product["title"];
    
                // create p element for price
                const price = document.createElement("p");
                price.innerText = "$" + product["price"];
                
    
                card.appendChild(img);
                card.appendChild(category);
                card.appendChild(title);
                card.appendChild(price);
    
                // append card to container
                productDataContainer.appendChild(card);
            });

            // remove the loader animation
            productDataContainer.classList.remove("loader");
        }
        
        return;
    })
    .catch((error) => {
        window.location.href = "./error.html";
        console.error('Error', error);
    });

